const express = require('express')
const router = new express.Router()
const User = require('../models/user')
const bcrypt = require('bcryptjs')
const auth = require('../middleware/auth')
const sharp = require('sharp')
const multer = require('multer')

router.post('/users', async (req, res) =>{
    const user = new User(req.body)
    try{
        await user.save()
        const token = await user.generateAuthToken()
        res.status(201).send({user, token})
    } catch(e){
        res.status(400).send(e)
    }
})

router.post('/users/login', async(req,res)=>{
    try{
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.send({user, token})
    }catch(e){
        res.status(400).send(e.message)
    }
})

router.post('/users/logout', auth,async(req, res)=>{
    try {        
        req.user.tokens = req.user.tokens.filter((token)=>{
            return token.token !== req.token
        })
        console.log('going on')
        await req.user.save()
        res.send('u r log out')
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/users/logoutall', auth, async(req, res) =>{
    try {
        req.user.tokens = []
        await req.user.save()
        res.status(200)
        res.send('now u dropped all tokens')
    } catch (error) {
        res.status(500).send(error)
    }
})

router.get('/users/me', auth,async (req, res) =>{
    try{
        res.send(req.user)
    }catch(e){
        res.status(500).send()
    }
})


router.patch('/users/me', auth,async (req, res) =>{
    const updates = Object.keys(req.body)
    const allowedUpdate = ['name', 'email', 'age', 'password']
    const isValidOperation = updates.every((update)=> allowedUpdate.includes(update) )
    if(!isValidOperation){
        return res.status(400).send({error: 'Invalid updates'})
    }

    try {
        // const user = await User.findById(req.user._id)

        updates.forEach((update)=> req.user[update] = req.body[update])

        await req.user.save()
        res.send(req.user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/users/me', auth, async (req, res) =>{
    try {
        await req.user.remove()
        
        res.send(req.user)
    } catch (e) {
        res.status(500).send(e)
    }
})

const upload = multer({
    limits: {
        fileSize: 1000000},
    fileFilter(req, file, callback){
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            return callback(new Error('pls upload img or gtfo'))
        }
        callback(undefined, true)
    }
})

router.post('/users/me/avatar', auth, upload.single('avatar'), async(req, res) =>{
    try {
        // req.user.avatar = req.file.buffer

        const buffer = await sharp(req.file.buffer).resize({width: 300, height: 300}).png().toBuffer()
        req.user.avatar = buffer
        await req.user.save()
        res.status(200).send()
    } catch (e) {
        res.status(500).send(e)
    }
}, (error, req, res, next) =>{
    res.status(400).send({error: error.message})
})

router.delete('/users/me/avatar', auth, async(req, res)=>{
    try {
        req.user.avatar = undefined
        await req.user.save()
        res.status(200).send('avatar deleted')
    } catch (e) {
        res.status(500).send(e.message)
    }
})

router.get('/users/:id/avatar', async(req, res)=>{
    try {
        const user = await User.findById(req.params.id)
        if (!user || !user.avatar) {
            throw new Error('user doesnt exist, or no avatar')
        }

        res.set('Content-Type', 'image/png')
        res.send(user.avatar)

    } catch (error) {
        res.status(404).send('Cant find user')
    }
})

module.exports = router