const mongoose = require('mongoose')
const validator = require('validator')

const taskShema = new mongoose.Schema({
    description: {
        trim: true,
        type: String,
        required: true
    },
    completed:{
        type: Boolean,
        default: false
    },
    owner:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
},
    {timestamps: true}
)

const Task = mongoose.model('Task', taskShema)



module.exports = Task