
require('../src/db/mongoose')
const User = require('../src/models/user')
const Task = require('../src/models/task')


// ObjectId("5c87f091d1c4552c7d27a783")


const deleteTaskAndCount = async (id) =>{
    const taskDelete = await Task.findByIdAndRemove(id)
    const count = await Task.countDocuments({completed: false})
    return (count)
}

deleteTaskAndCount('5c87f091d1c4552c7d27a783').then((count)=>{
    console.log(`count of inclompleted tasks : ${count}`)
}).catch((e)=>{
    console.log(e)
})

// User.findByIdAndUpdate('5c8d4ec3a19a890ef54cced5', {age: 10}).then((user) =>{
//     console.log(user)
//     return User.countDocuments({age: 10})
// }).then((userCount)=>{
//     console.log(userCount)
// }).catch((e)=>{
//     console.log(e)
// })

// const updateAgeAndCount = async (id, age) =>{
//     const user = await User.findByIdAndUpdate(id, {age})
//     const count = await User.countDocuments({age})
//     return count
// }

// updateAgeAndCount('5c8d39f915f2eb0a219620c2', 20).then((count)=>{
//     console.log(count)
// }).catch((e)=>{
//     console.log(e)
// })
