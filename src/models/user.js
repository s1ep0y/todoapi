const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Task = require('./task')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email:{
        trim: true,
        unique: true,
        lowercase: true,
        type: String,
        required: true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Email is invaild')
            }
        }
    },
    age:{
        type: Number,
        default: 0,
        validate(value){
            if(value < 0){
                throw new Error('Age must be a possitive number')
            }
        }
    },
    password:{
        required: true,
        type: String,
        trim: true,
        minlength: 7,
        validate(value){
            if(value.toLowerCase().includes('password')){
                throw new Error('password cant containt "password"')
            }
        }
    },
    tokens:[{
        token:{
            type: String,
            required: true
        }
    }],
    avatar:{
        type: Buffer
    }
},
{
    timestamps: true
}
    )

userSchema.virtual('tasks', {
    ref: 'Task',
    localField: '_id',
    foreignField: 'owner'
})

userSchema.methods.generateAuthToken = async function(){
    const user = this
    const token = jwt.sign({_id: user._id.toString()} ,'thisIsMySecret')

    user.tokens = user.tokens.concat({token})
    await user.save()

    return token
}

userSchema.methods.toJSON = function (){
    const user = this 
    const userObj = user.toObject()

    delete userObj.password
    delete userObj.tokens
    delete userObj.avatar
    return userObj
}

userSchema.statics.findByCredentials = async(email, password)=>{
    const user = await User.findOne({email})
    if(!user){
        throw new Error('Unable to login. Wrong password or user doesnt exist')
    }
    const isMatch = await bcrypt.compare(password, user.password)

    if(!isMatch){
        throw new Error('Unable to login. Wrong password or user doesnt exist')
    }
    return user
}

userSchema.pre('save', async function(next){
    const user = this

    if (user.isModified('password')){
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

userSchema.pre('delete', async function(next) {
    const user = this
    await Task.deleteMany({owner: user._id})    
    
    next()
})


const User = mongoose.model('User', userSchema)

module.exports = User